package com.cellcom.prototype.controller;

import com.cellcom.prototype.entity.CustomerSupport;
import com.cellcom.prototype.repository.CustomerSupportRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class LoginController {

    private CustomerSupportRepository customerSupportRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public LoginController(CustomerSupportRepository customerSupportRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.customerSupportRepository = customerSupportRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody CustomerSupport customerSupport) {
        customerSupport.setPassword(bCryptPasswordEncoder.encode(customerSupport.getPassword()));
        customerSupportRepository.save(customerSupport);
    }
}
