package com.cellcom.prototype.repository;

import com.cellcom.prototype.entity.CustomerSupport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerSupportRepository extends JpaRepository<CustomerSupport, Long> {
    CustomerSupport findByEmail(String email);
}
